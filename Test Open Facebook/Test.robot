*** Settings ***
Library           AppiumLibrary
Resource          PageKeyword.txt
Resource          PageRepo.txt
Resource          PageVariable.txt

*** Test Cases ***
FaceRecog
    Open Application    http://192.168.1.120:4723/wd/hub    platformName=Android    platformVersion=7.1.1    deviceName=a54b408e    appPackage=com.idpassglobal.aisfacerecog_pvt    appActivity=com.idpassglobal.aisfacerecog.WelcomeActivity
    ...    automationName=UiAutomator2    #noReset=true
    ${status}    Run Keyword And Return Status    Wait Until Page Contains Element    ${microphone}
    Run Keyword If    '${status}'=='True'    Login
    ...    ELSE    Log    bypass
    Comment    AppiumLibrary.Wait Until Page Contains Element    ${originno}    ${timeout}    #เบอร์เดิม

EasyApp
    Open Application    http://192.168.1.120:4723/wd/hub    platformName=Android    platformVersion=7.1.1    deviceName=a54b408e    appPackage=com.idpassglobal.aisappeasy.pvtapi    appActivity=com.idpassglobal.aisappeasy.MainActivity
    ...    automationName=UiAutomator2
    AppiumLibrary.Wait Until Page Contains Element    ${ssl}    ${timeout}    #ssl certificate
    AppiumLibrary.Click Element    ${ok}    #ปุ่ม ok
    AppiumLibrary.Wait Until Page Contains Element    ${phoneno}    ${timeout}    #ช่องกรอกเบอร์
    AppiumLibrary.Click Element    ${phoneno}    #ช่องกรอกเบอร์
    AppiumLibrary.Input Text    ${phoneno}    0925385989    #ช่องกรอกเบอร์
    AppiumLibrary.Wait Until Page Contains Element    ${confirm}    ${timeout}    #ปุ่มตกลง
    Comment    AppiumLibrary.Click Element    ${confirm}    #ปุ่มตกลง
